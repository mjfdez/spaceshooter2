﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet3 : Bullet {
public Vector3 iniPos;

	void Start(){
		iniPos = transform.position;
	}

	public override void Shot(Vector3 position, float direction){
		transform.position = position;
		shooting = true;
		transform.rotation = Quaternion.Euler(0, 25, direction);
	}
	
}
