﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpaceManager : MonoBehaviour
{

    public void Resume()
    {
        Debug.Log("RESUME");
        Application.Quit();
    }

    public void Menu()
    {
        Debug.Log("MENU");
        SceneManager.LoadScene("MainMenu");
    }

    public void Update()
    {

    }

}
